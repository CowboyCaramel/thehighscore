package main

import (
	"encoding/json"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/gorilla/handlers"

	"github.com/gorilla/mux"
)

// Score Struct
type Score struct {
	ID           int `json:"id"`
	TheBestScore int `json:"TheBestScore"`
}

var score []Score

// Give the best score
func getTheBestScore(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	// json.NewEncoder(w).Encode(score)
	x := score
	json.NewEncoder(w).Encode(x)
	return
}

// Give a score with some ID
func getTheBestScoreId(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	vars := mux.Vars(r)

	for _, thescore := range score {
		if strconv.Itoa(thescore.ID) == vars["id"] {
			json.NewEncoder(w).Encode(thescore)
			return
		}
	}

	json.NewEncoder(w).Encode(&Score{})
}

// Update the best score
func updateTheBestScore(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	vars := mux.Vars(r)

	var tempScore Score
	for index, thescore := range score {
		if strconv.Itoa(thescore.ID) == vars["id"] {
			_ = json.NewDecoder(r.Body).Decode(&tempScore)
			tempScore.ID = index
			score[index] = tempScore
			json.NewEncoder(w).Encode(score[index])
			return
		}
	}

	json.NewEncoder(w).Encode(&Score{})
}
func loggingMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Println(r.RequestURI)

		defer func(startedAt time.Time) {
			log.Println(r.Method, r.Proto, r.RequestURI, time.Since(startedAt))
		}(time.Now())

		next.ServeHTTP(w, r)
	})
}

func main() {

	// Initialize the router
	router := mux.NewRouter().StrictSlash(true)
	// Initializing with some dummy data
	score = append(score, Score{ID: 0, TheBestScore: 4})

	// Handling the endpoints
	router.HandleFunc("/api/thebestscore", getTheBestScore).Methods("GET")
	router.HandleFunc("/api/thebestscore/{id}", getTheBestScoreId).Methods("GET")
	router.HandleFunc("/api/thebestscore/{id}", updateTheBestScore).Methods("PUT")
	http.Handle("/", router)

	router.Use(loggingMiddleware)
	// Running the Server
	headersOk := handlers.AllowedHeaders([]string{"X-Requested-With"})
	originsOk := handlers.AllowedOrigins([]string{"*"})
	methodsOk := handlers.AllowedMethods([]string{"GET", "HEAD", "PUT", "POST", "PUT", "OPTIONS"})

	log.Fatal(http.ListenAndServe(":"+os.Getenv("PORT"), handlers.CORS(originsOk, headersOk, methodsOk)(router)))

}
